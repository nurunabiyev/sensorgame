## About this game
Showcases accelerometer, gyroscope and magnetometer features of Android device withing a little ball-game.
The game starts by using accelerometer by default, but user can choose another method by clicking on Settings icon in lower right corner.
The goal is to drive a ball from 1 space to another through the labyrinth.

## Installation and Build
You can install APK directly from `/app/release/app-release.apk`. In order to build from scratch, you will need to install Android SDK and use Gradle (or from Android Studio Directly).