package com.nurunabiyev.sensorgame.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import kotlin.math.max
import kotlin.math.min

inline fun Context.toast(text: String?, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, "$text", length).show()
}

inline fun View.lineToPx(line: Float, allLines: Float) =
    max(0f, min((this.bottom - 1).toFloat(), line / allLines * this.bottom))

inline fun View.colToPx(col: Float, allCols: Float) =
    max(0f, min((this.right - 1).toFloat(), col / allCols * this.right))

inline fun View.pxToLine(px: Float, allLines: Float) =
    max(0f, min(allLines - 1, px / this.bottom * allLines))

inline fun View.pxToCol(px: Float, allCols: Float) =
    max(0f, min(allCols - 1, px / this.right * allCols))