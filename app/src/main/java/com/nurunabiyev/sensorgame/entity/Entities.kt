package com.nurunabiyev.sensorgame.entity

/**
 * Abstract coordination system which can support both Integer, and Float
 */
data class Coordinates(val x: Number, val y: Number)

/**
 * 3 possibilities to manipulate the ball
 *
 * [Acceleration] - User tilt's phone (just like in the game)
 * [Gyroscope] - User needs to perform abrupt rotations (angular momentum)
 * [Magnetometer] - Ball is manipulated by applying magnetic force on the phone's sensor
 */
enum class BallInputMode {
    Acceleration, Gyroscope, Magnetometer
}