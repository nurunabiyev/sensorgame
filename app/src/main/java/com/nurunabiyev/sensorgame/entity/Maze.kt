package com.nurunabiyev.sensorgame.entity

/**
 * Each maze contains a map and associated level
 * Example of a maze:
 *      --------
 *      -OOO-OOX
 *      -O-O-O--
 *      -O-O-O--
 *      -O-O-O--
 *      -O-O-O--
 *      EO-OOO--
 *      --------
 * Where user navigates from E (entrance) to X (exit) through path of O
 *  and avoiding walls indicated by '-'
 *
 */
class Maze(
    val level: Int,
    private val map: Array<CharArray>
) {
    val entry = getCoordinatesOf('E')
    val exit = getCoordinatesOf('X')
    val lines = map.size.toFloat()
    val columns = map[0].size.toFloat()

    fun getCurrentChar(x: Int, y: Int): Char = map[x][y]

    fun getRange(): IntRange = map[0].indices

    fun getCurrentLine(x: Int): CharArray = map[x]

    fun verify() {
        if (map.size != 8 || map.none { it.size == 8 }) {
            throw Exception("Incorrect map length")
        }
        map.forEach {
            it.forEach { char ->
                if (char != '-' && char != 'O' && char != 'E' && char != 'X') {
                    throw Exception("Incorrect map characters")
                }
            }
        }
    }

    fun getCoordinatesOf(char: Char): Coordinates {
        var x = -1
        var y = -1
        map.forEachIndexed { index, chars ->
            if (chars.contains(char)) {
                x = index
                y = chars.indexOf(char)
                return Coordinates(x, y)
            }
        }
        return Coordinates(x, y)
    }
}