package com.nurunabiyev.sensorgame.repository

import com.nurunabiyev.sensorgame.entity.Maze
import java.util.ArrayList

/**
 * Singleton that contains 4 levels and provides all maps
 * Please refer to [Maze] for an example of a map
 */
object MazeProvider {

    fun getAllLevels() = ArrayList<Maze>().apply {
        add(mazeLevel1)
        add(mazeLevel2)
        add(mazeLevel3)
        add(mazeLevel4)
    } as List<Maze>

    private val mazeLevel1 = Maze(
        level = 1,
        map = arrayOf(
            charArrayOf('-', '-', '-', '-', '-', '-', '-', '-'),
            charArrayOf('-', 'O', 'O', 'O', '-', 'O', 'O', 'X'),
            charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
            charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
            charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
            charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
            charArrayOf('E', 'O', '-', 'O', 'O', 'O', '-', '-'),
            charArrayOf('-', '-', '-', '-', '-', '-', '-', '-')
        )

    )

    private val mazeLevel2 = Maze(
        level = 2,
        map = arrayOf(
            charArrayOf('O', 'O', 'O', '-', '-', 'O', 'O', 'O'),
            charArrayOf('O', '-', 'O', '-', '-', 'O', '-', 'X'),
            charArrayOf('O', '-', 'O', 'O', 'O', 'O', '-', '-'),
            charArrayOf('O', '-', '-', '-', '-', '-', '-', '-'),
            charArrayOf('O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'),
            charArrayOf('-', '-', '-', '-', '-', '-', '-', 'O'),
            charArrayOf('E', 'O', 'O', 'O', 'O', 'O', 'O', 'O'),
            charArrayOf('-', '-', '-', '-', '-', '-', '-', '-')
        )
    )

    private val mazeLevel3 = Maze(
        level = 3,
        map = arrayOf(
            charArrayOf('-', '-', '-', 'O', 'O', 'O', '-', '-'),
            charArrayOf('-', 'O', '-', 'O', '-', 'O', 'O', 'X'),
            charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', 'O'),
            charArrayOf('-', 'O', 'O', 'O', '-', 'O', '-', 'O'),
            charArrayOf('-', 'O', '-', '-', '-', '-', 'O', 'O'),
            charArrayOf('-', 'O', 'O', 'O', '-', 'O', 'O', '-'),
            charArrayOf('E', 'O', '-', 'O', 'O', 'O', '-', '-'),
            charArrayOf('-', '-', '-', 'O', '-', '-', '-', '-')
        )
    )

    private val mazeLevel4 = Maze(
        level = 4,
        map = arrayOf(
            charArrayOf('E', 'O', 'O', 'O', 'O', 'O', 'O', '-'),
            charArrayOf('-', '-', 'O', 'O', 'O', 'O', 'O', 'O'),
            charArrayOf('X', '-', '-', 'O', '-', '-', '-', 'O'),
            charArrayOf('O', '-', '-', '-', '-', '-', 'O', 'O'),
            charArrayOf('O', 'O', '-', 'O', '-', 'O', 'O', '-'),
            charArrayOf('-', 'O', '-', '-', '-', '-', 'O', '-'),
            charArrayOf('-', 'O', '-', '-', 'O', 'O', 'O', '-'),
            charArrayOf('-', 'O', 'O', 'O', 'O', '-', '-', '-')
        )
    )

}