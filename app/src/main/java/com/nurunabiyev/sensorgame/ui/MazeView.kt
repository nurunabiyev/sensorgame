package com.nurunabiyev.sensorgame.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.view.View
import com.nurunabiyev.sensorgame.entity.Coordinates
import com.nurunabiyev.sensorgame.entity.Maze
import com.nurunabiyev.sensorgame.utils.colToPx
import com.nurunabiyev.sensorgame.utils.lineToPx
import com.nurunabiyev.sensorgame.utils.pxToCol
import com.nurunabiyev.sensorgame.utils.pxToLine
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sign


@SuppressLint("ViewConstructor")
class MazeView(
    context: Context,
    private val mCallback: Runnable,
    private val mMaze: Maze,
) : View(context) {

    init {
        mMaze.verify()
    }

    private val mEntry = mMaze.entry
    private val mExit = mMaze.exit

    private val mLines = mMaze.lines
    private val mCols = mMaze.columns

    private val mBgPaint = PaintProvider.getBackgroundPaint()
    private val mLinePaint = PaintProvider.getLinePaint()
    private val mBallPaint = PaintProvider.getBallPaint()
    private val mWallPaint = PaintProvider.getWallPaint()
    private val mBeginPaint = PaintProvider.getBeginPaint()
    private val mEndPaint = PaintProvider.getEndPaint()
    private val mTextPaint = PaintProvider.getTextPaint()

    private val mRectF = RectF(
        left.toFloat(),
        top.toFloat(),
        right.toFloat(),
        bottom.toFloat()
    )

    private var mXBall: Float = -1f
    private var mYBall: Float = -1f
    private val mBallRadius: Float
        get() {
            val unit = min(lineToPx(1f, mLines), colToPx(1f, mCols))
            return unit * 0.20f
        }

    private var mPrevTimestamp = 0L

    // ui game status
    private var mReady = false
    private var mFinished = false

    /**
     * Ball control mechanism
     */
    suspend fun updateBall(input: Coordinates, timestamp: Long) = withContext(Dispatchers.Default) {
        if (!mReady) return@withContext
        var x = input.x.toFloat()
        var y = input.y.toFloat()
        val millisFromLast = (timestamp - mPrevTimestamp) / 100000
        mPrevTimestamp = timestamp
        val ballRadius = mBallRadius
        val maxDelta = ballRadius * 2

        // modify for inclination
        if (abs(x) < 1.2) x = 0f else x -= (sign(x) * 1.2).toFloat()
        if (abs(y) < 1.2) y = 0f else y -= (sign(y) * 1.2).toFloat()
        if (x == 0f && y == 0f) return@withContext

        // calculate delta position
        var xBallDelta: Float
        var yBallDelta: Float
        val xDelta = max(-maxDelta, min(maxDelta, x * millisFromLast * 0.03f))
        val yDelta = max(-maxDelta, min(maxDelta, y * millisFromLast * 0.03f))
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            xBallDelta = yDelta
            yBallDelta = xDelta
        } else {
            xBallDelta = xDelta
            yBallDelta = yDelta
        }

        // update delta taking UI into account
        val lineMazePos = pxToLine(mYBall, mLines).toInt()
        val colMazePos = pxToCol(mXBall, mCols).toInt()
        if (x > 0) {
            val lineMazePosRadius = pxToLine(mYBall + mBallRadius + yBallDelta, mLines).toInt()
            if (lineMazePos != lineMazePosRadius
                && mMaze.getCurrentChar(lineMazePosRadius, colMazePos) == '-'
            ) {
                yBallDelta =
                    lineToPx(lineMazePosRadius.toFloat(), mLines) - mBallRadius - mYBall
            }
        } else {
            val lineMazePosRadius = pxToLine(mYBall - mBallRadius + yBallDelta, mLines).toInt()
            if (lineMazePos != lineMazePosRadius
                && mMaze.getCurrentChar(lineMazePosRadius, colMazePos) == '-'
            ) {
                yBallDelta = lineToPx(lineMazePos.toFloat(), mLines) + mBallRadius - mYBall
            }
        }
        if (y > 0) {
            val colMazePosRadius = pxToCol(mXBall + mBallRadius + xBallDelta, mCols).toInt()
            if (colMazePos != colMazePosRadius
                && mMaze.getCurrentChar(lineMazePos, colMazePosRadius) == '-'
            ) {
                xBallDelta = colToPx(colMazePosRadius.toFloat(), mCols) - mBallRadius - mXBall
            }
        } else {
            val colMazePosRadius = pxToCol(mXBall - mBallRadius + xBallDelta, mCols).toInt()
            if (colMazePos != colMazePosRadius
                && mMaze.getCurrentChar(lineMazePos, colMazePosRadius) == '-'
            ) {
                xBallDelta = colToPx(colMazePos.toFloat(), mCols) + mBallRadius - mXBall
            }
        }

        // perform move
        mYBall += yBallDelta
        mXBall += xBallDelta

        // up to screen boundaries + radius
        mXBall = max(mBallRadius, min(this@MazeView.right - mBallRadius, mXBall))
        mYBall = max(mBallRadius, min(this@MazeView.bottom - mBallRadius, mYBall))

        // check if exited this level
        if (mExit.x == pxToLine(mYBall, mLines).toInt()
            && mExit.y == pxToCol(mXBall, mCols).toInt()
        ) {
            mFinished = true
            withContext(Dispatchers.Main) { mCallback.run() }
        } else {
            withContext(Dispatchers.Main) { this@MazeView.invalidate() }
        }
    }

    /**
     * Draw ball and labyrinth
     */
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // init
        if (mXBall == -1f) {
            mXBall = colToPx(mEntry.y.toInt() + 0.5f, mCols)
            mYBall = lineToPx(mEntry.x.toInt() + 0.5f, mLines)
        }

        // refresh board
        mRectF.left = left.toFloat()
        mRectF.top = top.toFloat()
        mRectF.right = right.toFloat()
        mRectF.bottom = bottom.toFloat()
        canvas.drawRect(mRectF, mBgPaint)
        canvas.drawRect(mRectF, mLinePaint)

        // add walls
        for (line in mMaze.getRange()) {
            val vLine = mMaze.getCurrentLine(line)
            for (col in vLine.indices) {
                if (vLine[col] == '-') {
                    drawSquares(line, col, canvas, mWallPaint)
                }
            }
        }

        // add begin / end
        drawSquares(mEntry.x.toInt(), mEntry.y.toInt(), canvas, mBeginPaint)
        drawSquares(mExit.x.toInt(), mExit.y.toInt(), canvas, mEndPaint)

        // add ball
        canvas.drawCircle(mXBall, mYBall, mBallRadius, mBallPaint)

        // show level info
        drawLevelNumber(canvas)

        // ready flag
        mReady = true
    }

    private fun drawSquares(line: Int, col: Int, canvas: Canvas, paint: Paint) {
        val startX = colToPx(col.toFloat(), mCols)
        val stopX = colToPx((col + 1).toFloat(), mCols)
        val startY = lineToPx(line.toFloat(), mLines)
        val stopY = lineToPx((line + 1).toFloat(), mLines)
        val mRectF = RectF(startX, startY, stopX, stopY)
        canvas.drawRect(mRectF, paint)
    }

    private fun drawLevelNumber(canvas: Canvas) {
        canvas.drawText("Lv. ${mMaze.level}", 10f, 60f, mTextPaint)
    }

}