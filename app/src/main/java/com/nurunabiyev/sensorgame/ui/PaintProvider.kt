package com.nurunabiyev.sensorgame.ui

import android.graphics.Color
import android.graphics.Paint

/**
 * Provides styling of maze elements (walls, ball, exit/entry points, etc)
 */
object PaintProvider {
    fun getBackgroundPaint() = Paint().apply {
        style = Paint.Style.FILL
        color = Color.rgb(204,204,147)
    }

    fun getLinePaint() = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        color = Color.BLUE
        strokeWidth = 5f
    }

    fun getWallPaint() = Paint().apply {
        style = Paint.Style.FILL
        color = Color.rgb(128,21,0)
    }

    fun getBeginPaint() = Paint().apply {
        style = Paint.Style.FILL
        color = Color.rgb(255,251,230)
    }

    fun getEndPaint() = Paint().apply {
        style = Paint.Style.FILL
        color = Color.rgb(13,77,0)
    }

    fun getBallPaint() = Paint().apply {
        style = Paint.Style.FILL
        color = Color.rgb(0,106,128)
    }

    fun getTextPaint() = Paint().apply {
        color = Color.WHITE
        textSize = 60f
    }
}