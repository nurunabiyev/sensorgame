package com.nurunabiyev.sensorgame

import androidx.lifecycle.ViewModel
import com.nurunabiyev.sensorgame.entity.Maze
import com.nurunabiyev.sensorgame.repository.MazeProvider

/**
 * Lifecycle aware game manager
 */
class GameViewModel : ViewModel() {
    private var currentLevel = 0
    private var maps: List<Maze> = MazeProvider.getAllLevels()

    init {
        resetLevel()
    }

    fun getCurrentMaze(): Maze {
        return maps[currentLevel - 1]
    }

    /**
     * Move to next level, and return if has won
     */
    fun performMove(): Boolean {
        return if (hasWon()) {
            resetLevel()
            true
        } else {
            increment()
            false
        }
    }

    private fun increment() = currentLevel++

    private fun hasWon() = currentLevel == maps.size

    private fun resetLevel() {
        currentLevel = 1
    }
}