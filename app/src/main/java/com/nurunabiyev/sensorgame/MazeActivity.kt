package com.nurunabiyev.sensorgame

import android.content.DialogInterface
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.Button
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.nurunabiyev.sensorgame.entity.BallInputMode
import com.nurunabiyev.sensorgame.entity.Coordinates
import com.nurunabiyev.sensorgame.ui.MazeView
import com.nurunabiyev.sensorgame.utils.toast
import kotlinx.android.synthetic.main.activity_maze.*
import kotlinx.coroutines.launch


class MazeActivity : AppCompatActivity(), SensorEventListener {
    private lateinit var mSensorManager: SensorManager
    private var mAccelerometer: Sensor? = null
    private var mMagnetometer: Sensor? = null
    private var mGyroscope: Sensor? = null
    private var mMazeView: MazeView? = null
    private var mSelectedBallMode = BallInputMode.Acceleration

    private val gameViewModel: GameViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maze)

        mSensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)

        if (mAccelerometer == null) toast("No accelerometer available")
        if (mMagnetometer == null) toast("No magnetometer available")
        if (mGyroscope == null) toast("No gyroscope available")

        settingsIV.setOnClickListener { ballInputDialog() }
        makeGameView()
    }

    private fun ballInputDialog() {
        var mAlertDialog: AlertDialog? = null
        val dialogBuilder = AlertDialog.Builder(this).apply {
            setCancelable(true)
            setPositiveButton("Cancel") { _: DialogInterface, _: Int -> }
        }

        val view = layoutInflater.inflate(R.layout.dialog_mode, null)
        dialogBuilder.setView(view)
        val bAcc = view.findViewById<Button>(R.id.accelerationModeB)
        val bGyro = view.findViewById<Button>(R.id.gyroModeB)
        val bMagnet = view.findViewById<Button>(R.id.magnetModeB)
        bAcc.setOnClickListener {
            mSelectedBallMode = BallInputMode.Acceleration
            toast("Tilt your phone")
            mAlertDialog?.cancel()
        }
        bGyro.setOnClickListener {
            mSelectedBallMode = BallInputMode.Gyroscope
            toast("Use angular momentum") // todo
            mAlertDialog?.cancel()
        }
        bMagnet.setOnClickListener {
            mSelectedBallMode = BallInputMode.Magnetometer
            toast("Approach to metal/magnet")
            mAlertDialog?.cancel()
        }

        mAlertDialog = dialogBuilder.create()
        mAlertDialog.setTitle("Select input mode:")
        mAlertDialog.show()
    }

    private fun makeGameView() {
        mMazeView = MazeView(this, { startNextLevel() }, gameViewModel.getCurrentMaze())
        linearLayout?.removeAllViews()
        linearLayout?.addView(mMazeView)
    }

    private fun startNextLevel() {
        if (gameViewModel.performMove()) toast("You Won! Restarting...")
        makeGameView()
    }

    override fun onPause() {
        super.onPause()
        mSensorManager.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST)
        mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST)
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_FASTEST)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val x = event?.values?.get(0) ?: return
        val y = event.values?.get(1) ?: return

        val input: Coordinates? = when {
            isAccelerometer(event) -> Coordinates(x, y)
            isGyroscope(event) -> Coordinates(-y, x)
            isMagnetometer(event) -> Coordinates(x / 250.0, y / 250.0)
            else -> null
        }
        lifecycleScope.launch { input?.let { mMazeView?.updateBall(it, event.timestamp) } }
    }

    private fun isMagnetometer(event: SensorEvent?): Boolean {
        return event?.sensor?.type == Sensor.TYPE_MAGNETIC_FIELD
                && mSelectedBallMode == BallInputMode.Magnetometer
    }

    private fun isGyroscope(event: SensorEvent?): Boolean {
        return event?.sensor?.type == Sensor.TYPE_GYROSCOPE
                && mSelectedBallMode == BallInputMode.Gyroscope
    }

    private fun isAccelerometer(event: SensorEvent?): Boolean {
        return event?.sensor?.type == Sensor.TYPE_ACCELEROMETER
                && mSelectedBallMode == BallInputMode.Acceleration
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

}