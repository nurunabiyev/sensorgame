package com.nurunabiyev.sensorgame

import com.nurunabiyev.sensorgame.entity.Coordinates
import com.nurunabiyev.sensorgame.entity.Maze
import org.junit.Test

import org.junit.Assert.*

class ExampleUnitTest {

    @Test
    fun gameLogicTest() {
        val gameViewModel = GameViewModel()

        assert(gameViewModel.getCurrentMaze().level == 1)
        assert(!gameViewModel.performMove())

        assert(gameViewModel.getCurrentMaze().level == 2)
        assert(!gameViewModel.performMove())

        assert(gameViewModel.getCurrentMaze().level == 3)
        assert(!gameViewModel.performMove())

        // level 4 - winning
        assert(gameViewModel.performMove())
    }

    @Test
    fun checkMazeCorrectness() {
        val mazeLevel1 = Maze(
            level = 1,
            map = arrayOf(
                charArrayOf('-', '-', '-', '-', '-', '-', '-', '-'),
                charArrayOf('-', 'O', 'O', 'O', '-', 'O', 'O', 'X'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('E', 'O', '-', 'O', 'O', 'O', '-', '-'),
                charArrayOf('-', '-', '-', '-', '-', '-', '-', '-')
            )
        )

        // will pass
        mazeLevel1.verify()

        val mazeLevel2 = Maze(
            level = 1,
            map = arrayOf(
                charArrayOf('x', '-', '-', '-', '-', '-', '-', '-'),
                charArrayOf('-', 'O', 'O', 'O', '-', 'O', 'O', 'X'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('-', 'O', '-', 'O', '-', 'O', '-', '-'),
                charArrayOf('E', 'O', '-', 'O', 'O', 'O', '-', '-'),
                charArrayOf('-', '-', '-', '-', '-', '-', '-', '-')
            )
        )
        assert(mazeLevel2.getCoordinatesOf('x') == Coordinates(0, 0))
        // will throw
        mazeLevel2.verify()

    }
}